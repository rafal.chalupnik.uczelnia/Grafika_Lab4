﻿using System;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using SharpGL.SceneGraph;

namespace Grafika_Lab4
{
    public partial class MainWindow : Window
    {
        private const float Step = 3.0f;

        private float verticalAngle = 30.0f;
        private float horizontalAngle = 30.0f;

        public MainWindow()
        {
            InitializeComponent();
        }
        
        public void LoadCameraButtonClick(object _sender, EventArgs _args)
        {
            var dialog = new OpenFileDialog { Filter = "Camera file (*.cam) | *.cam" };
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.FileName))
                Model.Instance.LoadCameraFromFile(dialog.FileName);
        }

        public void LoadSceneButtonClick(object _sender, EventArgs _args)
        {
            var dialog = new OpenFileDialog {Filter = "Scene file (*.brp) | *.brp"};
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.FileName))
                Model.Instance.SceneFilePath = dialog.FileName;
        }

        public void MainWindowKeyDown(object _sender, KeyEventArgs _args)
        {
            switch (_args.Key)
            {
                case Key.Up:
                    Model.Instance.VerticalAngle += Step;
                    break;
                case Key.Down:
                    Model.Instance.VerticalAngle -= Step;
                    break;
                case Key.Left:
                    Model.Instance.HorizontalAngle -= Step;
                    break;
                case Key.Right:
                    Model.Instance.HorizontalAngle += Step;
                    break;
                case Key.K:
                    Model.Instance.CameraRotation += Step;
                    break;
                case Key.L:
                    Model.Instance.CameraRotation -= Step;
                    break;
            }
        }

        public void OrthogonalXControlDraw(object _sender, OpenGLEventArgs _args)
        {
            Model.Instance.DrawOrthogonalX(_args.OpenGL);
        }

        public void OrthogonalYControlDraw(object _sender, OpenGLEventArgs _args)
        {
            Model.Instance.DrawOrthogonalY(_args.OpenGL);
        }

        public void OrthogonalZControlDraw(object _sender, OpenGLEventArgs _args)
        {
            Model.Instance.DrawOrthogonalZ(_args.OpenGL);
        }

        public void PerspectiveControlDraw(object _sender, OpenGLEventArgs _args)
        {
            Model.Instance.DrawPerspective(_args.OpenGL);
        }

        public void SaveCameraButtonClick(object _sender, EventArgs _args)
        {
            var dialog = new SaveFileDialog() { Filter = "Camera file (*.cam) | *.cam" };
            dialog.ShowDialog();
            if (!string.IsNullOrEmpty(dialog.FileName))
                Model.Instance.SaveCameraToFile(dialog.FileName);
        }
    }
}