﻿using System.Globalization;
using System.IO;
using System.Linq;
using SharpGL;

namespace Grafika_Lab4
{
    public class Model
    {
        #region SINGLETON

        private static Model instance_;

        public static Model Instance => instance_ ?? (instance_ = new Model());

        #endregion

        private string[] sceneFileContent_ = null;
        private string sceneFilePath_;
        private float verticalAngle_;

        private Model() { }

        private void Draw(OpenGL _openGl, float _rotateX, float _rotateY, float _rotateZ)
        {
            _openGl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            _openGl.LoadIdentity();

            _openGl.Translate(0f, 0.0f, -15.0f);
            _openGl.Rotate(_rotateX, _rotateY, _rotateZ);
            _openGl.Begin(OpenGL.GL_TRIANGLES);
            _openGl.Enable(OpenGL.GL_LIGHTING);
            _openGl.Enable(OpenGL.GL_LIGHT0);
            _openGl.Enable(OpenGL.GL_COLOR_MATERIAL);
            _openGl.Enable(OpenGL.GL_DEPTH_TEST);
            float[] position = {5.0f, 5.0f, 5.0f, 1.0f};
            _openGl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, position);
            float[] blue1 = new [] { 0.4f, 0.4f, 0.6f, 1f };
            float[] blue2 = new [] { 0.0f, 0f, 0.8f, 1f };
            float[] blue3 = new [] { 0.0f, 0f, 0.15f, 1f };
            _openGl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, blue1);
            _openGl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, blue2);
            _openGl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, blue3);
            float[] ambientLevel = { 0.15f, 0.15f, 0.15f, 1.0f };
            _openGl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, ambientLevel);
            _openGl.ColorMaterial(OpenGL.GL_COLOR_MATERIAL_FACE, OpenGL.GL_SPECULAR);
            _openGl.ShadeModel(OpenGL.GL_SMOOTH);

            if (!string.IsNullOrEmpty(sceneFilePath_))
                LoadSceneFromFile(_openGl);

            _openGl.End();
            _openGl.Flush();
        }

        public float CameraRotation;
        public float HorizontalAngle;
        public string SceneFilePath
        {
            get => sceneFilePath_;
            set
            {
                sceneFilePath_ = value;
                if (!string.IsNullOrEmpty(sceneFilePath_))
                    sceneFileContent_ = File.ReadAllLines(sceneFilePath_);
            }
        }
        public float VerticalAngle
        {
            get => verticalAngle_;
            set
            {
                if (value >= -90.0f && value <= 90.0f)
                    verticalAngle_ = value;
            }
        }

        public void DrawOrthogonalX(OpenGL _openGl)
        {
            Draw(_openGl, 0, 0, 0);
        }
        public void DrawOrthogonalY(OpenGL _openGl)
        {
            Draw(_openGl, 0, 90, 0);
        }
        public void DrawOrthogonalZ(OpenGL _openGl)
        {
            Draw(_openGl, 90, 0, 0);
        }
        public void DrawPerspective(OpenGL _openGl)
        {
            Draw(_openGl, VerticalAngle, HorizontalAngle, CameraRotation);
        }
        public void LoadCameraFromFile(string _filePath)
        {
            var content = File.ReadAllLines(_filePath).Where(_line => _line.Length != 0 && _line[0] != '#').ToList();
            HorizontalAngle = float.Parse(content[0], CultureInfo.InvariantCulture);
            VerticalAngle = float.Parse(content[1], CultureInfo.InvariantCulture);
            CameraRotation = float.Parse(content[2], CultureInfo.InvariantCulture);
        }
        public void LoadSceneFromFile(OpenGL _openGl)
        {
            var fileContentEnumerator = sceneFileContent_.Where(_line => _line.Length != 0 && _line[0] != '#').GetEnumerator();
            fileContentEnumerator.MoveNext();

            var verticesNumber = int.Parse(fileContentEnumerator.Current);
            fileContentEnumerator.MoveNext();

            var vertices = new float[verticesNumber, 3];
            for (var i = 0; i < verticesNumber; i++)
            {
                var vertexData = fileContentEnumerator.Current.Split('\t');
                for (var j = 0; j < 3; j++)
                    vertices[i, j] = float.Parse(vertexData[j], CultureInfo.InvariantCulture);
                fileContentEnumerator.MoveNext();
            }

            var trianglesNumber = int.Parse(fileContentEnumerator.Current);
            fileContentEnumerator.MoveNext();

            var trianglesVertices = new int[trianglesNumber, 3];
            for (var i = 0; i < trianglesNumber; i++)
            {
                var triangleData = fileContentEnumerator.Current.Split('\t');
                for (var j = 0; j < 3; j++)
                    trianglesVertices[i, j] = int.Parse(triangleData[j]);
                fileContentEnumerator.MoveNext();
            }

            var partsNumber = int.Parse(fileContentEnumerator.Current);
            fileContentEnumerator.MoveNext();

            var trianglesParts = new int[trianglesNumber];
            for (var i = 0; i < trianglesNumber; i++)
            {
                trianglesParts[i] = int.Parse(fileContentEnumerator.Current);
                fileContentEnumerator.MoveNext();
            }

            var partsParameters = new float[partsNumber, 3];
            for (var i = 0; i < partsNumber; i++)
            {
                var partData = fileContentEnumerator.Current.Split('\t');
                for (var j = 0; j < 3; j++)
                    partsParameters[i, j] = float.Parse(partData[j], CultureInfo.InvariantCulture);
                fileContentEnumerator.MoveNext();
            }

            for (var i = 0; i < trianglesNumber; i++)
            {
                var part = trianglesParts[i];
                //_openGl.Material(OpenGL.GL_FRONT_AND_BACK, OpenGL.GL_AMBIENT_AND_DIFFUSE, new[]
                //{
                //    partsParameters[part, 0],
                //    partsParameters[part, 1],
                //    partsParameters[part, 2]
                //});
                //_openGl.Color(partsParameters[part, 0], partsParameters[part, 1], partsParameters[part, 2]);
                

                _openGl.Normal(
                    (vertices[trianglesVertices[i, 1], 1] - vertices[trianglesVertices[i, 0], 1]) * (vertices[trianglesVertices[i, 2], 2] - vertices[trianglesVertices[i, 0], 2]) 
                        - (vertices[trianglesVertices[i, 1], 2] - vertices[trianglesVertices[i, 0], 2]) * (vertices[trianglesVertices[i, 2], 1] - vertices[trianglesVertices[i, 0], 1]),
                    (vertices[trianglesVertices[i, 1], 2] - vertices[trianglesVertices[i, 0], 2]) * (vertices[trianglesVertices[i, 2], 0] - vertices[trianglesVertices[i, 0], 0])
                    - (vertices[trianglesVertices[i, 1], 0] - vertices[trianglesVertices[i, 0], 0]) * (vertices[trianglesVertices[i, 2], 2] - vertices[trianglesVertices[i, 0], 2]),
                    (vertices[trianglesVertices[i, 1], 0] - vertices[trianglesVertices[i, 0], 0]) * (vertices[trianglesVertices[i, 2], 1] - vertices[trianglesVertices[i, 0], 1])
                    - (vertices[trianglesVertices[i, 1], 1] - vertices[trianglesVertices[i, 0], 1]) * (vertices[trianglesVertices[i, 2], 0] - vertices[trianglesVertices[i, 0], 0]));

                float[] purple = { 0.6f, 0, 0.6f, 1 };
                float[] white = { 0.4f, 0.4f, 0.4f, 1 }; // For specular highlights.
                _openGl.Material(OpenGL.GL_FRONT, OpenGL.GL_AMBIENT_AND_DIFFUSE, purple);  // front material
                _openGl.Material(OpenGL.GL_FRONT, OpenGL.GL_SPECULAR, white);
                _openGl.Material(OpenGL.GL_FRONT, OpenGL.GL_SHININESS, 64);

                for (var j = 0; j < 3; j++)
                    _openGl.Vertex(vertices[trianglesVertices[i, j], 0], vertices[trianglesVertices[i, j], 1], vertices[trianglesVertices[i, j], 2]);
            }
        }
        public void SaveCameraToFile(string _filePath)
        {
            var fileContent = new string[3];
            fileContent[0] = HorizontalAngle.ToString(CultureInfo.InvariantCulture);
            fileContent[1] = VerticalAngle.ToString(CultureInfo.InvariantCulture);
            fileContent[2] = CameraRotation.ToString(CultureInfo.InvariantCulture);
            File.WriteAllLines(_filePath, fileContent);
        }
    }
}